FROM  maven:3.8.6-openjdk-8 as builder 

RUN mkdir /app

WORKDIR /app

COPY pom.xml . 

COPY src ./src

RUN mvn clean install package



# Use an official OpenJDK as the base image
FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the Spring Boot JAR file into the container
COPY --from=builder /app/target/devOpsDemo-0.0.1-SNAPSHOT.jar app.jar

# Expose the port that the Spring Boot app listens on
EXPOSE 2222

# Command to run the Spring Boot application
CMD ["java", "-jar", "app.jar"]

