# DevOps-Example
This is a sample Spring Boot Application, used to explain the Jenkins pipeline, in creating a full CI/CD flow using docker too.

# Jenkins 
Jenkins is an open source automation server written in Java. Jenkins helps to automate the non-human part of the software development process,
 with continuous integration and facilitating technical aspects of continuous delivery. It is a server-based system that runs in servlet containers 
 such as Apache Tomcat.
 
